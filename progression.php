<?php

$getFromStr = function() use ($argv){

	$str = isset($argv['1'])?$argv['1']:null;
	
	if(!isset($str)){
		$rez = "progression string is required";
		return $rez;
	}//
	
	if(!preg_match("/^[0-9,]+$/", $str)){
		$rez = "not valid string";
		return $rez;
	}//

	$rez = "not progression";
	$arr = explode(',', $str);
	$sum = array_sum($arr);
	$prod = array_product($arr);
	
	if(	$sum == ($arr[0] + $arr[count($arr)-1])*count($arr)/2 || 
		$prod == pow($arr[0] * $arr[count($arr)-1], count($arr)/2))
	{
		$rez = "is progression";
	}//
		
	return $rez;
};
		
print($getFromStr().PHP_EOL);

?>